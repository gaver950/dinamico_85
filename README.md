# Removidos
### lista_categoria.php
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista de Categoria</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_categoria">
        <tr>
            <th id="cod">Código</th>
            <th id="cat">Categoria</th>
            <th id="ati">Ativo</th>
            <th id="opc" colspan="2">Opções</th>
        </tr>
        <?php
            require_once('op_categoria.php');
            $categorias_retornadas = listar_categoria();
            if(count($categorias_retornadas)>0){
                foreach($categorias_retornadas as $categoria){
        ?>
        <tr>
            <td><?php echo $categoria['id_categoria'];?></td>
            <td><?php echo $categoria['categoria'];?></td>
            <td><?php echo $categoria['cat_ativo']=='1'?'Sim':'Não'; ?></td>
            <td style="text-align: center; font-size: 2; font: verdana, arial; color: white;"><a href="Principal.php?link="><i>Alterar</i></a></td>
            <td style="text-align: center; font-size: 2; font: verdana, arial; color: white;"><a href="Principal.php?link="><i>Excluir</i></a></td>
        </tr>
    </table>
    <?php } } ?>
</body>
</html>

### Classe noticia

<?php
    class Noticia{
        // Atributos
        private $id_noticia;
        private $id_categoria;
        private $titulo_noticia;
        private $img_noticia;
        private $visita_noticia;
        private $data_noticia;
        private $noticia_ativo;
        private $noticia;
        
        // Métodos de acesso (Getters and Setters)
        public function getIdNoticia(){
            return $this->id_noticia;
        }
        public function setIdNoticia($value){
            $this->id_noticia = $value;
        }
        public function getIdCat(){
        return $this->id_categoria;
        }
        public function setIdCat($value){
            $this->id_categoria = $value;
        }
        public function getTitulo(){
            return $this->titulo_noticia;
        }
        public function setTitulo($value){
            $this->titulo_noticia = $value;
        }
        public function getImg(){
            return $this->img_noticia;
        }
        public function setImg($value){
            $this->img_noticia = $value;
        }
        public function getVisita(){
            return $this->visita_noticia;
        }
        public function setVisita($value){
            $this->visita_noticia = $value;
        }
        public function getDatanoticia(){
            return $this->data_noticia;
        }
        public function setDatanoticia($value){
            $this->data_noticia = $value;
        }
        public function getAtivo(){
            return $this->noticia_ativo;
        }
        public function setAtivo($value){
            $this->noticia_ativo = $value;
        }
        public function getNoticia(){
            return $this->noticia;
        }
        public function setNoticia($value){
            $this->noticia = $value;
        }


        public function loadById($_id){
            $sql = new Sql();
            $results = $sql->select('SELECT * FROM noticia WHERE id_noticia = :id', array(':id'=>$_id));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select('SELECT * FROM noticia order by titulo_noticia');
        }
        public static function search($titulo_noticia){
            $sql = new Sql();
            return $sql->select('SELECT * FROM noticia WHERE titulo_noticia LIKE :titulo', array(':titulo'=>'%'.$titulo_noticia.'%'));
        }
        public function setData($data){
            $this->setIdNoticia($data['id_noticia']);
            $this->setIdCat($data['id_categora']);
            $this->setTitulo($data['titulo_noticia']);
            $this->setImg($data['img_noticia']);
            $this->setVisita($data['visita_noticia']);
            $this->setDatanoticia($data['data_noticia']);
            $this->setAtivo($data['noticia_ativo']);
            $this->setNoticia($data['noticia']);
        }
        public function insert(){
            $sql = new Sql();
            $results = $sql->select('CALL sp_noticia_insert(:categoria,:titulo,:img,:visita,:data,:ativo,:noticia)', array(
                ':categoria'=>$this->getIdCat(),
                ':titulo'=>$this->getTitulo(),
                'img'=>$this->getImg(),
                ':vista'=>$this->getVisita(),
                ':data'=>$this->getDatanoticia(),
                ':ativo'=>$this->getAtivo(),
                ':noticia'=>$this->getNoticia()
            ));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }
        public function update($_id, $_titulo, $_img, $_visita, $_datanoticia, $_ativo, $_noticia){
            $sql = new Sql();
            $sql->query('UPDATE noticia SET titulo_noticia=:titulo, img_noticia=:img, visitas=:visita, data_noticia=:data, ativo=:ativo, noticia=:noticia WHERE id_noticia=:id', array(
                ':id'=>$_id,
                ':titulo'=>$_titulo,
                ':img'=>$_img,
                ':visita'=>$_visita,
                ':data'=>$_datanoticia,
                ':ativo'=>$_ativo,
                ':noticia'=>$_noticia
            ));
        }
        public function delete(){
            $sql = new Sql();
            $sql->query('DELETE FROM noticia WHERE id_noticia=:id', array(':id'=>$this->getIdNoticia()));
        }
        public function __construct($_id = '', $_categoria='', $_titulo = '', $_img = '', $_visita = '', $_datanoticia = '', $_ativo = '', $_noticia = ''){
            $this->id_noticia = $_id;
            $this->id_categoria = $_categoria;
            $this->titulo_noticia = $_titulo;
            $this->img_noticia = $_img;
            $this->visitas = $_visita;
            $this->data_noticia = $_datanoticia;
            $this->noticia_ativo = $_ativo;
            $this->noticia = $_noticia;
        }
        public function updateVisita($_id){
            $sql = new Sql();
            $sql->query("UPDATE noticias set visita_noticia = visita_noticia +1 WHERE id_noticia = :id", array(':id'=>$_id));
        }
    }
?>

# dinamico_85
projeto da UC 12

## Preparação do ambiente 25 / 09 / 2019

## Criação de pastas e principais da arvore

#'select * from ´tabela´ where nome like %'+Nome+'%';
_____________________________________________________________________________________________________________________

# Manual

### git init |> torna a pasta em uma pasta git;

### git remote add origin https://gaver950@bitbucket.org/gaver950/root.git |> adciona o arquivo em um repositório no bitbucket (ou github);

### git pull origin master |> trás os arquivos que estão salvos no bitbucket;

### git push -f origin master |> envia ou da update nos arquivos no repositório;
### = |> atribuie valor;

### == |> compara valor

### === |> compara tipo e valor;

### && |> adciona uma segunda regra;

### $ |> quando esta com um nome qualquer ele se torna uma variavel;

### var_dump |> trás informações de uma variavel.;

### ** |> numero elevado elevado;

### % (mod) |> só reconhece numeros inteiros;

### PDO Php Data Object (Objeto de Dados PHP) |> tratamento de choque avançado;

### :: |> chama um método estático da aplicação;

### => |> associa valores;

### -> |> da uma propriedade;