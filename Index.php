<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/Style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <title>Document</title>
</head>
<body>
    <header id="estrutura">
        <nav id="topo" class="espacamento"></nav>
        <nav id="menu">
            <ul>
                <li><a href="index.php?link=1"><i class="fa fa-home">Home</i></a></li>
                <li><a href="#"><i class="fa fa-wrench">Serviços</i></a></li>
                <li><a href="index.php?link=3"><i class="fa fa-dropbox">Produtos</i></a></li>
                <li><a href="#"><i class="fa fa-phone-square">Contatos</i></a></li>
                <li><a href="admin/index.php"><i class="fa fa-user">Acesso a áre administrativa</i></a></li>
            </ul>
        </nav>
        <nav id="banner" class="banner"></nav>
        <section id="corpo">
            <nav id="esquerda" class="esquerda">
                <h1>Produtos</h1>
                <li><a href="Index.php?link="><i>Produto 1</i></a></li>
                <li><a href="Index.php?link="><i>Produto 2</i></a></li>
                <li><a href="Index.php?link="><i>Produto 3</i></a></li>
                <li><a href="Index.php?link="><i>Produto 4</i></a></li>
            </nav>
            <nav id="centro" class="centro">
                <?php
                    $link = $_GET['link'];
                    $pag[1] = "home.php";
                    $pag[3] = "produto.php";
                    $pag[4] = "conteudo_noticia.php";
                    if (!empty($link)){
                        if(file_exists($pag[$link])){
                            include($pag[$link]);
                        }
                        else{
                            include($pag[1]); // mostre o home
                        }
                    }
                    else{
                        include($pag[1]); // mostre o home
                    }
                ?>
            </nav>
            <nav id="direita" class="direita">
                <div id="noticias">
                    <?php
                        require_once('Config.php');
                        $noticias = Noticia::getList();
                        foreach ($noticias as $not) {
                            if ($not['noticia_ativo']==1) {
                    ?>
                    <h3><img src="<?php echo $not['img_noticia']; ?>" alt=""></h3>
                    <h1><?php echo $not['data_noticia']; ?></h1>
                    <a href="Index.php?link=4&idnoticia=<?php echo $not['id_noticia']; ?>">
                        <?php echo $not['titulo_noticia']; ?>
                    </a>
                    <?php }} ?>
                </div>
                <hr>
                <div id="post">
                    <?php
                        require_once('Config.php');
                        $user = new Usuario();
                        if ($user->getLog() == true) {
                                $posts = Post::getList();
                                foreach ($posts as $post) {
                                    if ($post['post_ativo']==1) {
                    ?>
                    <h3><img src="<?php echo $post['img_post']; ?>" alt=""></h3>
                    <h1><?php echo $post['data_post']; ?></h1>
                    <a href="Index.php?link=4&idpost=<?php echo $post['id_post']; ?>">
                        <?php echo $post['titulo_post']; ?>
                    </a>
                    <?php }}} 
                        else {
                            echo '<a href="admin/frm_login.php">Logar-se para poder ver os posts</a>';
                        }
                    
                    ?>  
                </div>
            </nav>
        </section>
        <footer id="rodape" class="container bg-gradient">
            <div class="social-icons">
                <a href="https://www.facebook.com/gabriel.vergilio.5" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="#" target="_blank"><i class="fa fa-google"></i></a>
                <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                <a href="#" target="_blank"><i class="fa fa-envelope"></i></a>
            </div>
            <p class="copyright">
                Copyright © Dos Titios.ltda 2019. Todos os direitos reservados.
            </p>
        </footer>
    </header>
</body>
</html>