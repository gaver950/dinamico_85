<?php
    $id = filter_input(INPUT_GET, 'id');
    $nome = filter_input(INPUT_GET, 'nome');
    $email = filter_input(INPUT_GET, 'email');
    $login = filter_input(INPUT_GET, 'login');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Alteração de Admin</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <form action="op_administrador.php" method="POST" enctype="multipart/form-data">
        <fieldset>
            <legend>Alteração de Admin</legend>
            <div>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
            </div>
            <div>
                <label for="">Nome</label>
              <input type="text" name="nome" value="<?php echo $nome; ?>">
            </div>
            <br>
            <div>
                <label for="">E-mail</label>
                <input type="email" name="email" value="<?php echo $email; ?>">
            </div>
            <br>
            <div>
                <label for="">Login</label>
                <input type="text" name="login" value="<?php echo $login; ?>">
            </div>
            <br>
            <div>
                <input type="submit" name="btn_alterar_adm" value="Salvar" class="botao">
            </div>
        </fieldset>
    </form>
</body>