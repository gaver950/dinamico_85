<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ADM</title>
    <link rel="stylesheet" href="css/Style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <section id="box-login">
        <nav id="formulario-login">
            <form id="frmlogin" name="frm_login" action="op_user.php" method="post">
                <fieldset>
                    <legend>Faça o Login - Usuário</legend>
                    <label for=""><span>E-mail</span></label>
                    <input type="email" name="txt_email" id="txt_email" >
                    <label for=""><span>Senha</span></label>
                    <input type="password" name="txt_senha" id="txt_senha" >
                    <input type="submit" name="btn_logar" id="btn_logar" value="logar" class="botao">
                    <input type="submit" name="btn_register" id="btn_register" value="Não é Cadastrado? Clique Aqui!" class="botao-comprido">
                    <br>
                    <span><?php echo (isset($_GET['msg']))?$_GET['msg']:""; ?></span>
                </fieldset>
            </form>
        </nav>
    </section>
</body>
</html>