<?php
    require_once('../Config.php');
    // Inserir Administrador
    if (isset($_POST['btn_cadastrar_adm'])) {
        // var_dump($_POST);
        $adm = new Administrador(
            $_POST['nome'],
            $_POST['email'],
            $_POST['login'],
            $_POST['senha']
        );
        if ($_POST['senha']==$_POST['confirma_senha']) {
            $adm->insert();
            if ($adm->getId()!=null) {
                header('location:Principal.php?link=10&msg=ok');
            }
        }
        
    }
    // Excluir Administrador
    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir');
    if(isset($id) && $excluir==1){
        $admin = new Administrador();
        $admin->setId($id);
        $admin->delete();
        header('location:principal.php?link=11&msg=ok');
    }
    if (isset($_POST['btn_alterar'])) {
        $adm = new Administrador();
        $adm->update($_POST['id'],$_POST['nome'],$_POST['email'],$_POST['login']);
        header('location:Principal.php?link=11&msg=ok');
    }

    // Login
    if (isset($_POST['btn_logar'])) {
        $adm = new Administrador();
        $adm->efetuarlogin($_POST['txt_login'], $_POST['txt_senha']);
        if ($adm->getId()>0) {
            header('location:Principal.php');
        }
        elseif ($adm->getId()==0) {
            header('location:Index.php?msg=erro');
        }
        $txt_login = isset($_POST['txt_login'])?$_POST['txt_login']:'';
        $txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';
    
        if (empty($txt_login) || empty($txt_senha)) {
            echo 'preencha os dados corretamente';
            exit;
        }
        /*$adm->efetuarlogin($txt_login,$txt_senha);
        if (!isset($adm->getId())) {
            echo 'preencha os dados corretamente';
            exit;
        }*/
        // verificando quem ta on
        $_SESSION['logado']=true;
        $_SESSION['id_adm']=$adm->getId();
        $_SESSION['nome_adm']=$adm->getNome();
        $_SESSION['login_adm']=$adm->getLogin();
        header('Location: Principal.php?link=1');
    }
    // Adeus
    if(isset($_GET['sair'])==true){
        $_SESSION['logado'] = false;
        $_SESSION['id_adm'] = null;
        $_SESSION['nome_adm'] = null;
        $_SESSION['login'] = null;
        header('Location: ../Index.php?link=1');
    }
?>