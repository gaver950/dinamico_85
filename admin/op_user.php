<?php
    require_once('../Config.php');
    // Inserir Usuario
    if (isset($_POST['btn_cadastrar_user'])) {
        // var_dump($_POST);
        $img = upload_imagem();

        $user = new Usuario(
            null,
            $_POST['nome'],
            $_POST['email'],
            $img[0],$_POST['foto'],
            $_POST['senha'],
            false
        );

        if (isset($img[0]) && $_POST['senha'] == $_POST['confirma_senha']) {
            $user->insert();
            if ($user->getId()!=null) {
                header('location:frm_login.php?msg=deu_bom');
            }
            else {
                header('location:frm_login.php?msg=deu_ruim');
            }
        } 
    }

    if (isset($_POST['btn_register'])) {
        header('Location:frm_user.php');
    }
    // Excluir Usuario
    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir');
    if(isset($id) && $excluir==1){
        $userin = new Usuario();
        $userin->setId($id);
        $userin->delete();
        header('location:principal.php?link=11&msg=ok');
    }
    if (isset($_POST['btn_alterar'])) {
        $user = new Usuario();
        $user->update($_POST['id'],$_POST['nome'],$_POST['email'],$_POST['login']);
        header('location:Principal.php?link=11&msg=ok');
    }

    // Login
    if (isset($_POST['btn_logar'])) {
        $user = new Usuario();
        $user->efetuarlogin($_POST['txt_email'], $_POST['txt_senha']);
        if ($user->getId()>0) {
            $user->taOn();
            header('location:../Index.php?link=1');
        }
        elseif ($user->getId()==0) {
            header('location:frm_login.php?msg=_deu_ruim.com');
        }
        $txt_email = isset($_POST['txt_email'])?$_POST['txt_email']:'';
        $txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';
    
        if (empty($txt_email) || empty($txt_senha)) {
            echo 'preencha os dados corretamente';
            exit;
        }
    }
?>