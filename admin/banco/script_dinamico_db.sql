-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dinamico_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dinamico_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dinamico_db` DEFAULT CHARACTER SET latin1 ;
USE `dinamico_db` ;

-- -----------------------------------------------------
-- Table `dinamico_db`.`administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico_db`.`administrador` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `login` VARCHAR(100) NOT NULL,
  `senha` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico_db`.`banner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico_db`.`banner` (
  `id_banner` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo_banner` VARCHAR(255) NOT NULL,
  `link_banner` VARCHAR(255) NOT NULL,
  `img_banner` VARCHAR(150) NOT NULL,
  `alt` VARCHAR(255) NOT NULL,
  `banner_ativo` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id_banner`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico_db`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico_db`.`categoria` (
  `id_categoria` INT(11) NOT NULL AUTO_INCREMENT,
  `categoria` VARCHAR(150) NOT NULL,
  `cat_ativo` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id_categoria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico_db`.`noticias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico_db`.`noticias` (
  `id_noticia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` INT(11) NOT NULL,
  `titulo_noticia` VARCHAR(255) NOT NULL,
  `img_noticia` VARCHAR(100) NOT NULL,
  `visita_noticia` INT(11) NOT NULL,
  `data_noticia` DATE NOT NULL,
  `noticia_ativo` VARCHAR(1) NOT NULL,
  `noticia` TEXT NOT NULL,
  PRIMARY KEY (`id_noticia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico_db`.`post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico_db`.`post` (
  `id_post` INT(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` INT(11) NOT NULL,
  `titulo_post` VARCHAR(250) NOT NULL,
  `descricao_post` TEXT NOT NULL,
  `img_post` VARCHAR(200) NOT NULL,
  `visitas` INT(11) NOT NULL,
  `data_post` DATE NOT NULL,
  `post_ativo` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id_post`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico_db`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico_db`.`usuario` (
  `id` INT(11) NOT NULL,
  `nome` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `foto` VARCHAR(100) NOT NULL,
  `senha` VARCHAR(35) NOT NULL,
  `logado` BIT(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

USE `dinamico_db` ;

-- -----------------------------------------------------
-- procedure sp_adm_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_adm_insert`(
_nome varchar(200),
_email varchar(200),
_login varchar(100),
_senha varchar(50)
)
BEGIN
insert into administrador (nome, email, login, senha) values(
_nome,
_email,
_login,
_senha
);
select * from administrador where id = last_insert_id();
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_banner_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_banner_insert`(
	_titulo varchar(255),
    _link varchar(255),
    _img varchar(150),
    _alt varchar(255),
    _ativo varchar(1)
)
BEGIN
insert into banner values(
	null,
    _titulo,
     _img,
	_alt,
	_ativo
);
	select * from banner where id_banner = last_insert_id();
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_cat_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cat_insert`(
	_categoria varchar(150),
    _ativo bit
)
BEGIN
insert into categoria values(
	null,
    _categoria,
    _ativo
);
select * from categoria where last_insert_id();
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_noticias_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_noticias_insert`(
	_id_categoria int,
    _titulo varchar(255),
    _img varchar(100),
    _visita int,
    _data date,
    _ativo varchar(1),
    _noticia text
)
BEGIN
insert into noticia values(
	null,
    _id_categoria,
    _titulo,
    _img,
    _visita,
    now(),
    _ativo,
    _noticia
);
select * from noticia where last_insert_id();
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_post_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_post_insert`(
	_id_categoria int,
    _titulo varchar(250),
    _descricao text,
    _img varchar(200),
    _visita int,
    _data date,
    _ativo varchar(1)
)
BEGIN
insert into post values(
	null,
    _id_categoria,
    _titulo,
    _descricao,
    _img,
    _visita,
    _data,
    _ativo
);
select * from post where last_insert_id();
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_user_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_insert`(
    _nome varchar(250),
    _email text,
    _foto varchar(100),
    _senha varchar(35),
	_logado bit(1)
)
BEGIN
insert into post values(
	null,
	_nome,
	_email,
    _foto,
    _senha,
    _logado
);
select * from usuario where last_insert_id();
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
