<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Administrador</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div id="formulario-menor">
        <form action="op_usuario.php" name="frmusuario" id="frmusuario" method="POST">
            <fieldset>
                <input type="hidden" name="id" id="id">
                <label for="">
                    <span>Nome</span>
                    <input type="text" name="nome" id="txt_nome_adm" required>
                </label>
                <label for="">
                    <span>E-mail</span>
                    <input type="email" name="email" id="txt_email_adm" required>
                </label>
                <label for="">
                    <span>Foto</span>
                    <input type="file" name="foto" id="txt_login_adm" required>
                </label>
                <label for="">
                    <span>Senha</span>
                    <input type="password" name="senha" id="txt_senha_adm" required> 
                </label>
                <label for="">
                    <span>Comfirmar Senha</span>                
                    <input type="password" name="confirma_senha" id="confirma_senha_adm" required>
                </label>
                <label for="">
                    <input type="submit" name="btn_cadastrar_user" value="Cadastrar Usuário" class="botao">
                </label>
                <span><?php echo isset($_GET['msg'])?'Sucesso':''; ?></span>
            </fieldset>
        </form>
    </div>
</body>
</html>