<?php
    // require_once('../Conexao.php');
    class Sql extends PDO{
        private $cn;
        public function __construct(){
            $this->cn = new PDO("mysql:host=127.0.0.1;dbname=dinamico_db","root","");
            // $cn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }
        public function setParams($command, $params = array()){
            foreach ($params as $key => $value) {
                $this -> setParam($command, $key,$value);
            }
        }
        public function setParam($command,$key,$value){
            $command->bindParam($key,$value);
        }
        public function query($commandSql, $params = array()){
            $cmd = $this->cn->prepare($commandSql);
            $this->setParams($cmd, $params);
            $cmd->execute();
            return $cmd;
        }
        public function select($commandSql, $params = array()){
            $cmd = $this->query($commandSql,$params);
            return $cmd->fetchAll(PDO::FETCH_ASSOC);
       }
    }
?>