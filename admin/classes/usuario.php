<?php
    class Usuario{
        private $id_user;
        private $nome;
        private $email;
        private $foto;
        private $senha;
        private $log;

        public function getId(){
            return $this->id_user;
        }
        public function setId($value){
            $this->id_user = $value;
        }
        public function getNome(){
            return $this->nome;
        }
        public function setNome($value){
            $this->nome = $value;
        }
        public function getEmail(){
            return $this->email;
        }
        public function setEmail($value){
            $this->email = $value;
        }
        public function getFoto(){
            return $this->foto;
        }
        public function setFoto($value){
            $this->foto = $value;
        }
        public function getSenha(){
            return $this->senha;
        }
        public function setSenha($value){
            $this->senha = $value;
        }
        public function getLog(){
            return $this->log;
        }
        public function setLog($value){
            $this->log = $value;
        }

        public function setData($data){
            $this->id_user = setId($data['id']);
            $this->nome = setNome($data['nome']);
            $this->email = setEmail($data['email']);
            $this->foto = setFoto($data['foto']);
            $this->senha = setSenha($data['senha']);
            $this->log = setLog($data['logado']);
        }

        public function Insert(){
            $sql = new Sql();
            $results = $sql->select('CALL procedure sp_user_insert(:nome,:email,:foto,:senha,:logado)',
                array(
                    ':nome' => getNome(),
                    ':email' => getEmail(),
                    ':foto' => getFoto(),
                    ':senha' => md5($this->getSenha()),
                    ':logado' => false
                )
            );
            if (count($results) > 0) {
                $this->setData($results[0]);
            }
        }

        public function efetuarlogin($_email, $_senha){
            $sql = new Sql();
            $senha_cript = md5($_senha);
            $results = $sql->select('SELECT * FROM usuario WHERE email = :email, AND senha = :senha',
               array(':email'=>$_email, ':senha'=>$senha_cript));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }

        public function taOn(){
            $sql = new Sql();
            $sql->query('UPDATE usuario SET logado = :logado WHERE id = :id', array(':logado'=>true, ':id'=>getId()));
        }

        public function Tchau(){
            $sql = new Sql();
            $sql->query('UPDATE usuario SET logado = :logado WHERE id = :id', array(':logado' => false, ':id' => getId()));
        }

        public function Deleta(){
            $sql = new Sql();
            $sql->query('DELETE FROM usuario WHERE id = :id', array(':id' => getId()));
        }
    }
?>