<?php
    class Banner{
    private $id;
    private $tit_banner;
    private $link_banner;
    private $img_banner;
    private $alt_banner;
    private $banner_ativo;

    public function getId(){
        return $this->id;
    }
    public function setId($value){
    $this->id = $value;
    }
    public function getTit_Banner(){
        return $this->tit_banner;
    }
    public function setTit_Banner($value){
    $this->tit_banner = $value;
    }
    public function getLink_Banner(){
        return $this->link_banner;
    }
    public function setLink_Banner($value){
        $this->link_banner = $value;
    }
    public function getImg_Banner(){
        return $this->img_banner;
    }
    public function setImg_Banner($value){
        $this->img_banner = $value;
    }
    public function getAlt_Banner(){
        return $this->alt_banner;
    }
    public function setAlt_Banner($value){
    $this->alt_banner = $value;
    }
    public function getBanner_Ativo(){
        return $this->banner_ativo;
    }
    public function setBanner_Ativo($value){
    $this->banner_ativo = $value;
    }
    public function loadById($_id){
        $sql = new Sql();
        $results = $sql->select('SELECT * FROM banner WHERE id = :id', array(':id'=>$_id));
        if (count($results)>0){
            $this->setDados($results[0]);
        }
    }
    public static function getList(){
        $sql = new Sql();
        return $sql->select('SELECT * FROM banner order by titulo_banner');
    }
    public static function search($tit_banner){
        $sql = new Sql();
        return $sql->select('SELECT * FROM banner WHERE titulo_banner LIKE :tit_banner', array(':tit_banner'=>'%'.$tit_banner.'%'));
    }
    public function setDados($dados){
        $this->setId($dados['id']);
        $this->setTit_Banner($dados['titulo_banner']);
        $this->setLink_Banner($dados['link_banner']);
        $this->setAlt_Banner($dados['alt_banner']);
        $this->setBanner_ativo($dados['banner_ativo']);
    }
    public function insert(){
        $sql = new Sql();
        $results = $sql->select('CALL sp_banner_insert(:titulo_banner,:link_banner,:img_banner,:alt,:banner_ativo)', array(
            ':titulo_banner'=>$this->getTit_Banner(),
            ':link_banner'=>$this->getLink_Banner(),
            ':img_banner'=>$this->getImg_Banner(),
            ':alt'=>$this->getAlt_Banner(),
            ':banner_ativo'=>$this->getBanner_Ativo()
        ));
        if (count($results)>0) {
            $this->setDados($results[0]);
        }
    }
    public function update($_id, $_tit_banner, $_link_banner, $_img_banner, $_alt_banner, $_banner_ativo){
        $sql = new Sql();
        $sql->query('UPDATE banner SET 
            titulo_banner=:titulo_banner, 
            link_banner=:link_banner, 
            img_banner=:img_banner, 
            alt_banner=:alt_banner, 
            banner_ativo=:banner_ativo 
            WHERE id=:id', array(
                ':id'=>$_id,
                ':titulo_banner'=>$_tit_banner,
                ':link_banner'=>$_link_banner,
                ':img_banner'=>$_img_banner,
                ':alt_banner'=>$_alt_banner,
                ':banner_ativo'=>$_banner_ativo
            )
        );
    }
    public function delete(){
        $sql = new Sql();
        $sql->query('DELETE FROM banner WHERE id=:id', array(':id'=>$this->getId()));
    }
    public function __construct($_tit_banner='', $_link_banner='',$_img_banner='',$_alt_banner='',$_banner_ativo=''){
        $this->tit_banner = $_tit_banner;
        $this->link_banner = $_link_banner;
        $this->img_banner = $_img_banner;
        $this->alt_banner = $_alt_banner;
        $this->banner_ativo = $_banner_ativo;
    }
}
?>