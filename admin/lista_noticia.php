<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Noticia</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <table id="tb_noticia" width="100%" border="0" cellpadding="0" cellspacing="1" bg-color="#fcfcfc">
        <tr bg-color="#993300" text-align="center">
            <th width="10%" height="2">
                <font size="2" color="#fff">Categoria</font>
            </th>
            <th width="15%" height="2">
                <font size="2" color="#fff">Titulo</font>
            </th>
            <th width="10%" height="2">
                <font size="2" color="#fff">imagem</font>
            </th>
            <th width="7%" height="2">
                <font size="2" color="#fff">Visitas</font>
            </th>
            <th width="10%" height="2">
                <font size="2" color="#fff">Data</font>
            </th>
            <th width="10%" height="2">
                <font size="2" color="#fff">Ativo</font>
            </th>
            <th width="20%" height="2">
                <font size="2" color="#fff">Noticia</font>
            </th>
            <th colspan="2">
                <font size="2" color="#fff">Opções</font>
            </th>
        </tr>
        <?php
            require_once('../Config.php');
            $ret = Noticia::getList();
            foreach ($ret as $noticia){
        ?>
        <tr>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $noticia['id_categoria']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $noticia['titulo_noticia']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $noticia['img_noticia']; ?>
                </font>
            </td>                      
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $noticia['visita_noticia']; ?>
                </font>
            </td>            
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $noticia['data_noticia']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#c0c" style="margin-left: 40%;">
                    <?php echo $noticia['noticia_ativo']=='1'?'Sim':'Não'; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $noticia['noticia']; ?>
                </font>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php 
                            echo 'alterar_noticia.php?id='.$noticia['id_noticia'].
                            '&id_categoria='.$noticia['id_categoria'].
                            '$titulo_noticia'.$noticia['titulo_noticia'].
                            '&img_noticia'.$noticia['img_noticia'].
                            '&visita_noticia'.$noticia['visita_noticia'].
                            '&data_noticia'.$noticia['data_noticia'].
                            '&noticia_ativo'.$noticia['noticia_ativo'].
                            '&noticia'.$noticia['noticia'];
                        ?>" style="color: white; margin-left: 20%;">
                        <i class="fa fa-pencil"></i>Alterar
                    </a>
                </font>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php 
                            echo 'op_noticia.php?excluir=1&id='.$noticia['id'];
                        ?>" style="color: white; margin-left: 20%;">
                        <i class="fa fa-trash-o"></i>Excluir
                    </a>
                </font>
            </td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>