<?php
    require_once('../Config.php');
    if (isset($_POST['cadastro_usuario'])){
        $user = new Usuario(
            null,
            $_POST['nome'],
            $_POST['email'],
            $_POST['foto'],
            $_POST['senha'],
            false
        );
        if ($_POST['senha']==$_POST['confirma_senha']) {
            $user->Insert();
            if ($user->getId()!=null) {
                header('location:Principal.php?link=12&msg=ok');
            }
            else {
                header('location:Principal.php?link=12&msg=erro');
            }
        }
    }      
?>