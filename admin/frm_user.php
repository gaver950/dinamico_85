<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>userinistrador</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div id="formulario-menor">
        <form action="op_user.php" name="frm_user" id="frm_user" method="POST">
            <fieldset>
                <input type="hidden" name="id" id="id">
                <label for="">Nome</label>
                <input type="text" name="nome" id="txt_nome_user" required>
                <label for="">E-mail</label>
                <input type="email" name="email" id="txt_email_user" required>
                <label for="">Foto</label>
                <input type="file" name="foto" id="txt_foto_user" required>
                <label for="">Senha</label>
                <input type="password" name="senha" id="txt_senha_user" required> 
                <label for="">Comfirmar Senha</label>
                <input type="password" name="confirma_senha" id="confirma_senha_user" required>
                <input type="submit" name="btn_cadastrar_user" value="Cadastrar" class="botao">
            </fieldset>
        </form>
    </div>
</body>
</html>