<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Administrador</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <table id="tb_admin" width="100%" border="0" cellpadding="0" cellspacing="1" bg-color="#fcfcfc">
        <tr bg-color="#993300" text-align="center">
            <th width="10%" height="2"><font size="2" color="#fff">Id</font></th>
            <th width="25%" height="2"><font size="2" color="#fff">Nome</font></th>
            <th width="25%" height="2"><font size="2" color="#fff">E-mail</font></th>
            <th width="20%" height="2"><font size="2" color="#fff">Login</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
            require_once('../Config.php');
            $ret = Administrador::getList();
            foreach ($ret as $adm){
        ?>
        <tr>
            <td>
                <font size="2" face="verdana, arial" color="#0cc" style="margin-left: 40%;">
                    <?php echo $adm['id']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0" style="margin-left: 8.6%;">
                    <?php echo $adm['nome']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0" style="margin-left: 10%;">
                    <?php echo $adm['email']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0" style="margin-left: 40%;">
                    <?php echo $adm['login']; ?>
                </font>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php 
                            echo 'alterar_administrador.php?id='.$adm['id'].'&nome='.$adm['nome'].'$email'.$adm['email'].'&login'.$adm['login']; 
                        ?>" style="color: white; margin-left: 20%;">
                    <i class="fa fa-pencil"></i>Alterar
                    </a>
                </font>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo 'op_administrador.php?excluir=1&id='.$adm['id']; ?>" style="color: white; margin-left: 20%;">
                        <i class="fa fa-trash-o"></i>Excluir
                    </a>
                </font>
            </td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>