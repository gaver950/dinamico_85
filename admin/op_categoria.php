<?php
    if(isset($_POST['txt_categoria'])){
        require_once('Conexao.php');
        $categoria = $_POST['txt_categoria'];
        $ativo = isset($_POST['check_ativo'])?'1':'0';
        $cmd = $cn->prepare('INSERT INTO categoria  (categoria, cat_ativo) VALUES (:cat, :ativ)');
        $cmd->execute(array(
            ':cat' => $categoria,
            ':ativ' => $ativo
        ));
        header('location:Principal.php?link=3&msg=ok');
    }

    function listar_categoria(){
        require_once('Conexao.php');
        $query = 'select * from categoria';
        $cmd = $cn->prepare($query);
        $cmd->execute();
        return $cmd->fetchAll(PDO::FETCH_ASSOC);
    }

    if (isset($_POST['btn_deleta_cat'])) {
        require_once('Conexao.php');
        $id = $_POST['id_categoria'];
        $cmd = $cn->prepare('DELETE FROM categoria where id_categoria = :id');
        $cmd->execute(array(':id'=>$id));
    }
?>