<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Administrador</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div id="formulario-menor">
        <form action="op_administrador.php" name="frmadministrador" id="frmadministrador" method="POST">
            <fieldset>
                <input type="hidden" name="id" id="id">
                <label for="">Nome</label>
                <input type="text" name="nome" id="txt_nome_adm" required>
                <label for="">E-mail</label>
                <input type="email" name="email" id="txt_email_adm" required>
                <label for="">Login</label>
                <input type="text" name="login" id="txt_login_adm" required>
                <label for="">Senha</label>
                <input type="password" name="senha" id="txt_senha_adm" required> 
                <label for="">Comfirmar Senha</label>
                <input type="password" name="confirma_senha" id="confirma_senha_adm" required>
                <input type="submit" name="btn_cadastrar_adm" value="Cadastrar Administrador" class="botao">
                <span><?php echo isset($_GET['msg'])?'Sucesso':''; ?></span>
            </fieldset>
        </form>
    </div>
</body>
</html>