<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>post</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <table id="tb_post" width="100%" border="0" cellpadding="0" cellspacing="1" bg-color="#fcfcfc">
        <tr bg-color="#993300" text-align="center">
            <th width="10%" height="2">
                <font size="2" color="#fff">
                    Código
                </font>
            </th>
            <th width="10%" height="2">
                <font size="2" color="#fff">
                    Categoria
                </font>
            </th>
            <th width="15%" height="2">
                <font size="2" color="#fff">
                    Titulo
                </font>
            </th>
            <!-- <th width="15%" height="2">
                <font size="2" color="#fff">
                    Descrição
                </font>
            </th> -->
            <th width="30%" height="2">
                <font size="2" color="#fff">
                    imagem
                </font>
            </th>
            <th width="10%" height="2">
                <font size="2" color="#fff">
                    Visitas
                </font>
            </th>
            <!-- <th width="10%" height="2">
                <font size="2" color="#fff">
                    Data
                </font> -->
            </th>
            <th width="10%" height="2">
                <font size="2" color="#fff">
                    Ativo
                </font>
            </th>
            
            <th colspan="2">
                <font size="2" color="#fff">
                    Opções
                </font>
            </th>
        </tr>
        <?php
            require_once('../Config.php');
            $ret = Post::getList();
            foreach ($ret as $post){
        ?>
        <tr>
            <td>
                <font size="2" face="verdana, arial" color="#0cc">
                    <?php echo $post['id_post']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $post['id_categoria']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $post['titulo_post']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $post['descricao_post']; ?>
                </font>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $post['img_post']; ?>
                </font>
            </td>            
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $post['img_post']; ?>
                </font>
            </td>            
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $post['visita_post']; ?>
                </font>
            </td>            
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $post['data_post']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#c0c" style="margin-left: 40%;">
                    <?php echo $post['post_ativo']=='1'?'Sim':'Não'; ?>
                </font>
            </td>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php 
                            echo 'alterar_post.php?id='.$post['id_post'].
                            '&id_categoria='.$post['id_categoria'].
                            '$titulo_post'.$post['titulo_post'].
                            '&descricao_post'.$post['descricao_post'].
                            '&img_post'.$post['img_post'].
                            '&visita_post'.$post['visita_post'].
                            '&data_post'.$post['data_post'].
                            '&post_ativo'.$post['post_ativo'];
                        ?>" style="color: white; margin-left: 20%;">
                    <i class="fa fa-pencil"></i>Alterar
                    </a>
                </font>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php 
                            echo 'op_administrador.php?excluir=1&id='.$post['id']; 
                        ?>" style="color: white; margin-left: 20%;">
                        <i class="fa fa-trash-o"></i>Excluir
                    </a>
                </font>
            </td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>