<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Notícia</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <div id="formulario-menor">
        <form action="op_noticia.php" name="frmnoticia" id="frmnoticia" method="POST">
            <fieldset>
                <label for="">
                    <span>Categoria</span>
                    <?php 
                        require_once('../config.php');
                        $cats = Categoria::getList();
                    ?>
                    <select name="id_categoria" id="categoria">
                        <?php 
                            foreach($cats as $cat){
                        ?>
                        <option value="<?php echo $cat['id_categoria'];?>">
                            <?php echo $cat['categoria'];?>
                        </option>
                        <?php } ?>
                    </select>
                </label>
                <label for="">
                    <span>Titulo da Notícia</span>
                    <input type="text" name="txt_titulo_noticia" id="txt_titulo_noticia">
                </label>
                <label for="">
                    <span>Imagem da Notícia</span>
                    <input type="file" name="img_noticia" id="img_noticia">
                </label>
                <label for="">
                    <span>Data da Notícia</span>
                    <input type="date" name="data_noticia" id="data_noticia">
                </label>
                <label for="">
                    <span>Notícia</span>
                    <input type="text" name="txt_noticia" id="txt_noticia">
                </label>
                
                    <span>Ativo</span>
                    <input type="checkbox" name="check_noticia" id="check_noticia">
                </label>
                <input type="submit" name="btn_cadastrar_not" value="Cadastrar Noticia" class="botao">
                <span><?php echo isset($_GET['msg'])?'Sucesso':''; ?></span>
            </fieldset>
        </form>
    </div>
</body>
</html>