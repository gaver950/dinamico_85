<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Post</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <nav id="formulario-menor">
        <form action="op_post.php" name="frmpost" id="frmpost" method="POST">
            <fieldset>
                <label for="">
                    <span>Categoria</span>
                    <?php 
                        require_once('../config.php');
                        $cats = Categoria::getList();
                    ?>
                    <select name="id_categoria" id="categoria">
                        <?php 
                            foreach($cats as $cat){
                        ?>
                        <option value="<?php echo $cat['id_categoria'];?>">
                            <?php echo $cat['categoria'];?>
                        </option>
                        <?php } ?>
                    </select>
                </label>
                <label for="">
                    <span>Titulo da Post</span>
                    <input type="text" name="txt_titulo_post" id="txt_titulo_post">
                </label>
                <label for="">
                    <span>Descrição</span>
                    <input type="text" name="txt_descricao_post" id="txt_descricao_post">
                </label>
                <label for="">
                    <span>Imagem da Post</span>
                    <input type="file" name="img_post" id="img_post">
                </label>
                <label for="">
                    <span>Data da Post</span>
                    <input type="date" name="data_post" id="data_post">
                </label>
                    <span>Ativo</span>
                    <input type="checkbox" name="check_post" id="check_post">
                </label>
                <input type="submit" name="btn_cadastrar_post" value="Cadastrar post" class="botao">
                <span><?php echo isset($_GET['msg'])?'Sucesso':''; ?></span>
            </fieldset>
        </form>
    </nav>
</body>
</html>