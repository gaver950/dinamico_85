<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista Banner</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <table id="tb_banner" width="100%" border="0" cellpadding="0" cellspacing="1" bg-color="#fcfcfc">
        <tr bg-color="#993300" text-align="center">
            <th width="10%" height="2">
                <font size="2" color="#fff">Código</font>
            </th>
            <th width="25%" height="2">
                <font size="2" color="#fff">Titulo do Banner</font>
            </th>
            <th width="35%" height="2">
                <font size="2" color="#fff">Imagem do Banner</font>
            </th>
            <th width="10%" height="2">
                <font size="2" color="#fff">Ativo</font>
            </th>
            <th colspan="2">
                <font size="2" color="#fff">Opções</font>
            </th>
        </tr>
        <?php
            require_once('../Config.php');
            $banners_retornados = Banner::getList();
            foreach($banners_retornados as $banner){
        ?>
        <tr>
            <td>
                <font size="2" face="verdana, arial" color="#0cc">
                    <?php echo $banner['id_banner']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $banner['titulo_banner']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#cc0">
                    <?php echo $banner['img_banner']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#c0c">
                    <?php echo $banner['banner_ativo']=='1'?'Sim':'Não'; ?>
                </font>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="principal.php?link=" style="color: white; margin-left: 20%;">
                        <i class="fa fa-pencil">Alterar</i>
                    </a>
                </font>
            </td>
            <td text-align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo 'op_banner.php?excluir=1&id='.$banner['id']; ?>" style="color: white; margin-left: 20%;">
                        <i class="fa fa-trash-o">Excluir</i>
                    </a>
                </font>
            </td>
        </tr>
        <?php } ?>
    </table>   
</body>
</html>